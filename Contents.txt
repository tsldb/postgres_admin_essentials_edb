 ## Postgres Administration Esentials
 ## Course Agenda 

 ## Module 1 - Introduction
 ## Module Objectives
 ## EDB Postgres Platform
 ## EDB Postgres Platform - DBMS Options
 ## EDB Postgres Platform - Tool Suites
 ## EDB Postgres Platform - Deployment Options 
 ## EDB Postgres Platform - Services & Support
 ## The EDB Postgres Platform - Subscriptions
 ## EDB Postgres Ark
 ## Database Platform For Digital Business
 ## Facts about PostgreSQL
 ## Facts about EDB Postgres Advanced Server
 ## EDB Postgres Advanced Server Version History
 ## Major Features of PostgreSQL
 ## Major Features of PostgreSQL (continued) 
 ## EDB Postgres Advanced Server
 ## EDB Postgres Advanced Server - Security
 ## EDB Postgres Advanced Server - Bundled Tools
 ## EDB Postgres Advanced Server - Performance
 ## EDB Postgres Advanced Server - Database Compatibility for Oracle
 ## EDB Technologies
 ## EDB DBaaS Framework For Hybrid Clouds
 ## General Database Limits
 ## Comon Database Object Names
 ## Module Summary

 ## Module 2 - System Architecture
 ## Module Objectives 
 ## Architectural Summary
 ## Proces and Memory Architecture
 ## Utility Proceses
 ## More Utility Proces
 ## Postmaster as Listener
 ## User Back End Process
 ## Respond to Client
 ## Demo -List of Processes
 ## Disk Read Buffering
 ## Disk Write Buffering
 ## Background Writer Cleaning Scan
 ## Write Ahead Logging (WAL)
 ## Transaction Log Archiving
 ## Commit and Checkpoint
 ## Statement Processing
 ## Physical Database Architecture
 ## Installation Directory Layout
 ## Database Cluster Data Directory Layout
 ## Physical Database Architecture
 ## Demo - Data Directory
 ## Sample - Data Directory Layout
 ## Page Layout 
 ## Page Structure 
 ## Module Summary

 ## Module 3 - EDB Postgres Advanced Server Installation
 ## Module Objectives 
 ## OS User and Permissions
 ## The enterprisedb User Acount
 ## Demo - Ad OS User
 ## Installation Options for EDB Postgres Advanced Server
 ## Graphical Wizard Installation
 ## Download the EDB Postgres Advanced Server Installer
 ## Demo - Download Wizard Installer
 ## Windows - EDB Postgres Advanced Server Installation Wizard
 ## Linux - Extract and Run the EDB Postgres Advanced Server Installer
 ## EDB Postgres Advanced Server Installation Wizard - Welcome
 ## EDB Postgres Advanced Server Installation Wizard - License
 ## EDB Postgres Advanced Server Installation Wizard - User Authentication
 ## EDB Postgres Advanced Server Installation Wizard - Installation Directory
 ## EDB Postgres Advanced Server Installation Wizard - Components
 ## EDB Postgres Advanced Server Installation Wizard - Additional Directories
 ## EDB Postgres Advanced Server Installation Wizard - Configuration Mode
 ## EDB Postgres Advanced Server Installation Wizard - Password
 ## EDB Postgres Advanced Server Installation Wizard - Additional Configuration
 ## EDB Postgres Advanced Server Installation Wizard - Server Utilization
 ## EDB Postgres Advanced Server Installation Wizard - Workload Profile
 ## EDB Postgres Advanced Server Installation Wizard - Advanced Configuration
 ## EDB Postgres Advanced Server Installation Wizard - Summary
 ## EDB Postgres Advanced Server Installation Wizard - Finish 
 ## Demo - Wizard Installation
 ## Acces StackBuilder Plus 
 ## StackBuilder Plus - Welcome
 ## StackBuilder Plus - Select Applications
 ## Demo - StackBuilder Plus
 ## Text Mode Wizard Installation
 ## Prepare for Text Mode Installation
 ## Text Mode Installation
 ## Text Mode Installation Steps
 ## Text Mode Installation Summary
 ## RPM Installation
 ## RPM Installation Pre-Requisites
 ## RPM Package Manager
 ## YUM Installation
 ## Configure Yum Package Manager
 ## Install EDB Postgres Advanced Server using Yum
 ## After Installation
 ## Setting Environmental Variables
 ## Example - Setting Environment Variables
 ## Demo - Setup Enviromental Variables
 ## Module Summary
 ## Lab Exercise - 1

 ## Module 4 - Database Clusters 
 ## Module Objectives 
 ## Database Clusters 
 ## Creating a Database Cluster 
 ## initdb Utility
 ## Example - initdb
 ## Starting a Database Cluster
 ## Connecting to a Database Cluster
 ## Reload a Database Cluster
 ## Stopping a Database Cluster
 ## Demo - Database Cluster
 ## View Cluster Control Information
 ## Module Summary
 ## Lab Exercise - 1

 ## Module 5 - Configuration 
 ## Module Objectives
 ## Setting Server Parameters
 ## The Server Parameter File - postgresql.conf
 ## Ways to Set Server Parameters
 ## ALTER SYSTEM
 ## Demo - Changing Server Parameters
 ## Connection Settings
 ## Security and Authentication Settings
 ## Memory Settings
 ## Dynatune Dynamic Tuning 
 ## EDB Resource Manager 
 ## Query Planner Settings
 ## Write Ahead Log Settings
 ## Write Ahead Log Settings (Continued)
 ## Where To Log
 ## When To Log
 ## What To Log
 ## Background Writer Settings
 ## Statement Behavior
 ## Parallel Query Scan Settings
 ## Vacuum Cost Settings
 ## Autovacuum Settings
 ## Configuration File Includes 
 ## Module Summary 
 ## Lab Exercise - 1
 ## Lab Exercise - 2 
 ## Lab Exercise - 3 
 ## Lab Exercise - 4

 ## Module 6 - Creating and Managing Databases
 ## Module Objectives
 ## Object Hierarchy
 ## Demo - Default Hierarchy
 ## What Is A Database?
 ## Creating Databases
 ## Example - Creating Databases
 ## Accessing a Database 
 ## Demo - Creating Databases
 ## Users
 ## Creating Users using psql or edb-psql
 ## Creating Users Using EDB*Plus
 ## Creating Users Using createuser
 ## User Profile Management
 ## Creating A User Profile
 ## Privileges
 ## GRANT Statement
 ## Example - GRANT Statement
 ## REVOKE Statement
 ## Example - REVOKE Statement
 ## Demo - Users and Permissions
 ## What Is A Schema? 
 ## Benefits of Schemas
 ## Creating Schemas
 ## Demo - Creating Schemas
 ## What Is A Schema Search Path?
 ## Determine The Schema Search Path
 ## Schema Search Path and Users
 ## Set The Schema Search Path 
 ## Demo - Search Path
 ## Object Ownership 
 ## Module Summary 
 ## Lab Exercise - 1
 ## Lab Exercise - 2
 ## Lab Exercise - 3
 ## Demo - Sample Tables
 ## Lab Exercise - 4

 ## Module 7 - User Tools - Command Line Interfaces
 ## Module Objectives
 ## Introduction to edb-psql
 ## Connecting to A Database
 ## Demo - Connecting to A Database
 ## Conventions
 ## On Startup... 
 ## Entering Commands
 ## Controlling Output
 ## Advanced Features - Variables
 ## Advanced Features - Special Variables
 ## Information Commands
 ## Information Commands (continued)
 ## Other Common edb-psql Meta Commands
 ## Help
 ## Demo - Meta Commands
 ## EDB*Plus
 ## Module Summary
 ## Lab Exercise - 1

 ## Module 8 - GUI Tools
 ## Module Objectives
 ## Overview
 ## Features
 ## Download PEM Client
 ## Installing PEM Client
 ## PEM Client - License Agreement
 ## PEM Client - Installation Directory
 ## PEM Client - Finish Installation
 ## Demo - Installing PEM Client
 ## Open Postgres Enterprise Manager Client
 ## PEM Client First Look
 ## Register A Database Cluster
 ## Common Connection Problems
 ## Changing A Server's Registration
 ## Viewing Data
 ## Filtering and Sorting Data
 ## Query Tool
 ## Query Tool - Change Connection
 ## Query Tool - Data Output
 ## Query Tool - Graphical Query Builder
 ## Query Tool - Explain 
 ## Query Tool - Messages 
 ## Query Tool - History 
 ## Databases
 ## Creating a Database
 ## Schemas
 ## Tables
 ## Tables - Adding Columns/Constraints
 ## Tables - Indexes
 ## Tables - Maintenance
 ## Tablespaces
 ## Roles
 ## Server Status
 ## Demo - PEM Client
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 1 (continued)

 ## Module 9 - Security 
 ## Module Objectives
 ## Authentication and Authorization
 ## Levels of Security
 ## pg_hba.conf - Acces Control
 ## pg_hba.conf Example
 ## Demo - pg_hba.conf
 ## Authentication Problems
 ## Row Level Security (RLS)
 ## Example - Row Level Security
 ## Example - Row Level Security (continued)
 ## Application Access 
 ## SQL/Protect
 ## EDB*Wrap
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 2 
 ## Lab Exercise - 3 
 ## Lab Exercise - 4

 ## Module 10 - SQL Primer
 ## Module Objectives
 ## Data Types
 ## Structured Query Language
 ## Demo - Data Types and SQL Commands
 ## Tables
 ## Constraints
 ## Types of Constraints
 ## Example - CREATE TABLE
 ## Example - SERIAL Data Type
 ## ALTER TABLE
 ## DROP TABLE
 ## Demo - Create, Drop and Alter Table
 ## Inserting Data
 ## Example - INSERT
 ## Demo - Insert
 ## Updating Rows
 ## Example - UPDATE
 ## Demo - Update
 ## How to Upsert in EDB Postgres Advanced Server
 ## Deleting Data 
 ## Example - DELETE
 ## Demo - Delete 
 ## Views
 ## Example - View
 ## Demo - Views
 ## Materialized Views
 ## Materialized View Example
 ## Sequences
 ## Demo - Sequences
 ## Domains
 ## Example - Domain
 ## Demo - Domains
 ## Quoting
 ## Demo - Quoting
 ## Using SQL Functions 
 ## Format Functions
 ## Concatenating Strings
 ## Nested SELECT Statements
 ## Inner Joins
 ## Column and Table Aliases
 ## Outer Joins
 ## Indexes
 ## Example Index
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 2
 ## Lab Exercise - 3
 ## Lab Exercise - 4
 ## Lab Exercise - 5

 ## Module 11 - Backup, Recovery and PITR
 ## Module Objectives
 ## Types of Backup
 ## Database SQL Dump
 ## pg_dump Options
 ## Demo - SQL Dump Backups
 ## SQL Dump - Large Databases
 ## Restore - SQL Dump
 ## pg_restore Options
 ## Demo - SQL Dump Restore
 ## Entire Cluster - SQL Dump
 ## pg_dumpall Options
 ## Demo - Cluster Dump
 ## Backup - File System Level Backup
 ## Demo - Offline Filesystem Level Backups
 ## Backup - Continuous Archiving
 ## pg_receivexlog
 ## Continuous Archiving
 ## Demo - Continuous Archiving
 ## Base Backup Using Low Level API
 ## Base Backup Using pg_basebackup Tool
 ## Options for pg_basebackup
 ## pg_basebackup - Online Backup
 ## Demo - On Line Base Backups
 ## Point-in-Time Recovery
 ## Performing Point-in-Time Recovery
 ## Point-in-Time Recovery Settings
 ## Demo - Point-in-Time Recovery
 ## EDB Backup and Recovery Tool
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 2
 ## Lab Exercise - 3
 ## Lab Exercise - 4
 ## Lab Exercise -5
 ## Lab Exercise -6

 ## Module 12 - Routine Maintenance Tasks
 ## Module Objectives
 ## Database Maintenance
 ## Maintenance Tools
 ## Optimizer Statistics
 ## Example - Updating Statistics
 ## Demo - Optimizer Statistics
 ## Data Fragmentation and Bloat
 ## Routine Vacuuming
 ## Vacuuming Commands
 ## Vacuum and Vacuum Full
 ## VACUUM Syntax
 ## Example - Vacuuming
 ## Example - Vacuuming (continued)
 ## Demo - Vacuum Command
 ## Preventing Transaction ID Wraparound Failures
 ## Vacum Freeze
 ## The Visibility Map
 ## vacuumdb Utility
 ## Demo - vacuumdb Utility
 ## Autovacuuming
 ## Autovacuuming Parameters
 ## Per-Table Thresholds
 ## Routine Reindexing
 ## When to Reindex
 ## Demo - Reindexing
 ## CLUSTER
 ## Example - CLUSTER
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 2

 ## Module 13 - Data Dictionary
 ## Module Objectives
 ## The System Catalog Schema
 ## System Information Tables
 ## More System Information Tables
 ## System Information Functions
 ## System Administration Functions
 ## More System Administration Functions
 ## System Information Views
 ## Demo - System Catalogs
 ## EDB Postgres Advanced Server Oracle-Like Dictionaries
 ## Module Summary
 ## Lab Exercise - 1
 ## Lab Exercise - 2
 ## Lab Exercise - 3
 ## Lab Exercise - 4

 ## Module14 - Moving Data using COPY Command
 ## Module Objectives
 ## Loading Flat Files into Database Tables
 ## The COPY Command
 ## COPY TO
 ## Demo - COPY TO
 ## COPY FROM
 ## Example of COPY TO
 ## Example of COPY FROM
 ## Example - COPY Command on Remote Host
 ## Demo - COPY FROM
 ## COPY FREEZE
 ## Module Summary
 ## Lab Exercise - 1

 ## Module15 - EDB*Plus
 ## Module Objectives
 ## EDB*Plus
 ## EDB*Plus Features
 ## Starting EDB*Plus
 ## Demo - Starting EDB*Plus
 ## EDB*Plus SET Command
 ## EDB*Plus Commands
 ## List of EDB*Plus Commands
 ## Setup Login Variable
 ## Demo - Login Variables
 ## Calling a SQL Script
 ## Module Summary
 ## Lab Exercise

 ## Module16 - Tablespaces
 ## Module Objectives
 ## Tablespaces and Data Files
 ## Advantages of Tablespaces
 ## EDB Postgres Advanced Server Pre-Configured Tablespaces
 ## Creating Tablespaces
 ## Example - CREATE TABLESPACE
 ## Using Tablespaces
 ## Default and Temp Tablespace
 ## Altering Tablespaces
 ## Example - Alter Tabespace
 ## Dropping a Tablespace
 ## Example - Dropping Tablespaces
 ## Module Summary
 ## Lab Exercise

 ## Module 17 - Oracle Compatibility
 ## Module Objectives
 ## Oracle Compatibility
 ## Oracle Compatible Tools
 ## Oracle Compatibility Settings
 ## Example - Oracle Compatibility Settings
 ## Oracle Compatibility - Data Types
 ## Oracle Compatibility - Synonyms
 ## Dual Table
 ## Oracle Compatibility - Altering Session Parameters
 ## Oracle Compatibility - Database Link
 ## Oracle Compatibility - Create Directory
 ## Hierarchical Queries
 ## Oracle Compatible Connect By
 ## Table Partitioning
 ## Stored Procedure Language
 ## Built-in Packages
 ## System Catalog Views
 ## sys Schema
 ## ECPGPlus
 ## Open Client Library(OCL)
 ## Other Oracle Compatibilities
 ## Module Summary
 ## Lab Exercise
 ## Lab Exercise (continued)

 ## Module 18 - Migrating Oracle Objects to EDB Postgres Advanced Server
 ## Module Objectives
 ## Oracle to EDB Postgres Advanced Server Migration
 ## Migration Process
 ## Migration Assessment
 ## Assessing Migrations - Identify Candidate Databases
 ## Migration Assessment Methodology
 ## Database Migration Assessment Tool (DMAT)
 ## DMAT - Features
 ## DMAT - Architecture
 ## DMAT - Report
 ## Reading the Migration Assessment Report
 ## DMAT - Available Solutions
 ## Demo - DMAT
 ## Database Migration
 ## Database Migration - Preparation
 ## Information Collection
 ## EDB Postgres Migration Toolkit
 ## EDB Postgres Migration Toolkit - Features
 ## EDB Postgres Migration Toolkit - Objects Matrix
 ## EDB Postgres Migration Toolkit - Installation
 ## EDB Postgres Migration Toolkit - Graphical Installation
 ## EDB Postgres Migration Toolkit - StackBuilder Installation
 ## EDB Postgres Migration Toolkit - Select Target Server
 ## EDB Postgres Migration Toolkit - Application Selection
 ## EDB Postgres Migration Toolkit - Installation Steps
 ## Demo - Installing MTK
 ## EDB Postgres Migration Toolkit - Source-Specific Drivers
 ## Example - Source-Specific Drivers
 ## Demo -Source Specific Drivers
 ## EDB Postgres Migration Toolkit - Prepare a Connection
 ## EDB Postgres Migration Toolkit - Perform Migration
 ## EDB Postgres Migration Toolkit Options
 ## EDB Postgres Migration Toolkit Options (Continued)
 ## EDB Postgres Migration Toolkit - runMTK Command
 ## EDB Postgres Migration Toolkit - What's Migrated
 ## EDB Postgres Migration Toolkit - Offline Migration
 ## EDB Postgres Migration Toolkit - What's Migrated
 ## Demo - Migration
 ## Migration Toolkit - Looking for Errors
 ## Application Migration
 ## Application Migration
 ## Deployment
 ## Deployment
 ## Deploy - Postgres Enterprise Manager
 ## Ongoing Management and Optimization
 ## EDB Migration Methodology
 ## Module Summary
 ## Lab Exercise

 ## Module 19 - Data Loader
 ## Module Objectives
 ## Loading Flat Files into Database Tables
 ## EDB*Loader
 ## Data Loading Methods
 ## Invoking EDB*Loader
 ## The EDB*Loader Control File
 ## Control File Syntax
 ## EDB*Loader Example
 ## EDB*Loader Exit Codes
 ## Conclusion and Next Steps
 ## Module Summary
 ## Course Summary
 ## Next Steps
 ## EnterpriseDB Tools
 ## EDB Postgres for the Enterprise
 ## Thank You
