## Module 7 - User Tools - Command Line Interfaces
![](./images/edb_10181.png)
## Module Objectives
![](./images/edb_10182.png)
## Introduction to edb-psql
![](./images/edb_10183.png)
## Connecting to A Database
![](./images/edb_10184.png)
## Demo - Connecting to A Database
![](./images/edb_10185.png)
## Conventions
![](./images/edb_10186.png)
## On Startup...
![](./images/edb_10187.png)
## Entering Commands
![](./images/edb_10188.png)
## Controlling Output
![](./images/edb_10189.png)
## Advanced Features - Variables
![](./images/edb_10190.png)
## Advanced Features - Special Variables
![](./images/edb_10191.png)
## Information Commands
![](./images/edb_10192.png)
## Information Commands (continued)
![](./images/edb_10193.png)
## Other Common edb-psql Meta Commands
![](./images/edb_10194.png)
## Help
![](./images/edb_10195.png)
## Demo - Meta Commands
![](./images/edb_10196.png)
## EDB*Plus
![](./images/edb_10197.png)
## Module Summary
![](./images/edb_10198.png)
## Lab Exercise - 1
![](./images/edb_10199.png)
