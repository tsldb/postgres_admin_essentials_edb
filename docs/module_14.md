## Module14 - Moving Data using COPY Command
![](./images/edb_10390.png)
## Module Objectives
![](./images/edb_10391.png)
## Loading Flat Files into Database Tables
![](./images/edb_10392.png)
## The COPY Command
![](./images/edb_10393.png)
## COPY TO
![](./images/edb_10394.png)
## Demo - COPY TO
![](./images/edb_10395.png)
## COPY FROM
![](./images/edb_10396.png)
## Example of COPY TO
![](./images/edb_10397.png)
## Example of COPY FROM
![](./images/edb_10398.png)
## Example - COPY Command on Remote Host
![](./images/edb_10399.png)
## Demo - COPY FROM
![](./images/edb_10400.png)
## COPY FREEZE
![](./images/edb_10401.png)
## Module Summary
![](./images/edb_10402.png)
## Lab Exercise - 1
![](./images/edb_10403.png)