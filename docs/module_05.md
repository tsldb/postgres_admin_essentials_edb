## Module 5 - Configuration
![](./images/edb_10115.png)
## Module Objectives
![](./images/edb_10116.png)
## Setting Server Parameters
![](./images/edb_10117.png)
## The Server Parameter File - postgresql.conf
![](./images/edb_10118.png)
## Ways to Set Server Parameters
![](./images/edb_10119.png)
## ALTER SYSTEM
![](./images/edb_10120.png)
## Demo - Changing Server Parameters
![](./images/edb_10121.png)
## Connection Settings
![](./images/edb_10122.png)
## Security and Authentication Settings
![](./images/edb_10123.png)
## Memory Settings
![](./images/edb_10124.png)
## Dynatune Dynamic Tuning
![](./images/edb_10125.png)
## EDB Resource Manager
![](./images/edb_10126.png)
## Query Planner Settings
![](./images/edb_10127.png)
## Write Ahead Log Settings
![](./images/edb_10128.png)
## Write Ahead Log Settings (Continued)
![](./images/edb_10129.png)
## Where To Log
![](./images/edb_10130.png)
## When To Log
![](./images/edb_10131.png)
## What To Log
![](./images/edb_10132.png)
## Background Writer Settings
![](./images/edb_10133.png)
## Statement Behavior
![](./images/edb_10134.png)
## Parallel Query Scan Settings
![](./images/edb_10135.png)
## Vacuum Cost Settings
![](./images/edb_10136.png)
## Autovacuum Settings
![](./images/edb_10137.png)
## Configuration File Includes
![](./images/edb_10138.png)
## Module Summary
![](./images/edb_10139.png)
## Lab Exercise - 1
![](./images/edb_10140.png)
## Lab Exercise - 2
![](./images/edb_10141.png)
## Lab Exercise - 3
![](./images/edb_10142.png)
## Lab Exercise - 4
![](./images/edb_10143.png)
