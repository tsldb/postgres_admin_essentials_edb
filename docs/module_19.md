## Module 19 - Data Loader
![](./images/edb_10507.png)
## Module Objectives
![](./images/edb_10508.png)
## Loading Flat Files into Database Tables
![](./images/edb_10509.png)
## EDB*Loader
![](./images/edb_10510.png)
## Data Loading Methods
![](./images/edb_10511.png)
## Invoking EDB*Loader
![](./images/edb_10512.png)
## The EDB*Loader Control File
![](./images/edb_10513.png)
## Control File Syntax
![](./images/edb_10514.png)
## EDB*Loader Example
![](./images/edb_10515.png)
## EDB*Loader Exit Codes
![](./images/edb_10516.png)
## Conclusion and Next Steps
![](./images/edb_10517.png)
## Module Summary
![](./images/edb_10518.png)
## Course Summary
![](./images/edb_10519.png)
## Next Steps
![](./images/edb_10520.png)
## EnterpriseDB Tools
![](./images/edb_10521.png)
## EDB Postgres for the Enterprise
![](./images/edb_10522.png)
## Thank You
![](./images/edb_10523.png)