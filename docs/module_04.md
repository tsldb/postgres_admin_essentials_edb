## Module 4 - Database Clusters
![](./images/edb_10101.png)
## Module Objectives
![](./images/edb_10102.png)
## Database Clusters
![](./images/edb_10103.png)
## Creating a Database Cluster
![](./images/edb_10104.png)
## initdb Utility
![](./images/edb_10105.png)
## Example - initdb
![](./images/edb_10106.png)
## Starting a Database Cluster
![](./images/edb_10107.png)
## Connecting to a Database Cluster
![](./images/edb_10108.png)
## Reload a Database Cluster
![](./images/edb_10109.png)
## Stopping a Database Cluster
![](./images/edb_10110.png)
## Demo - Database Cluster
![](./images/edb_10111.png)
## View Cluster Control Information
![](./images/edb_10112.png)
## Module Summary
![](./images/edb_10113.png)
## Lab Exercise - 1
![](./images/edb_10114.png)
