## Module 3 - EDB Postgres Advanced Server Installation
![](./images/edb_10054.png)
## Module Objectives
![](./images/edb_10055.png)
## OS User and Permissions
![](./images/edb_10056.png)
## The enterprisedb User Acount
![](./images/edb_10057.png)
## Demo - Ad OS User
![](./images/edb_10058.png)
## Installation Options for EDB Postgres Advanced Server
![](./images/edb_10059.png)
## Graphical Wizard Installation
![](./images/edb_10060.png)
## Download the EDB Postgres Advanced Server Installer
![](./images/edb_10061.png)
## Demo - Download Wizard Installer
![](./images/edb_10062.png)
## Windows - EDB Postgres Advanced Server Installation Wizard
![](./images/edb_10063.png)
## Linux - Extract and Run the EDB Postgres Advanced Server Installer
![](./images/edb_10064.png)
## EDB Postgres Advanced Server Installation Wizard - Welcome
![](./images/edb_10065.png)
## EDB Postgres Advanced Server Installation Wizard - License
![](./images/edb_10066.png)
## EDB Postgres Advanced Server Installation Wizard - User Authentication
![](./images/edb_10067.png)
## EDB Postgres Advanced Server Installation Wizard - Installation Directory
![](./images/edb_10068.png)
## EDB Postgres Advanced Server Installation Wizard - Components
![](./images/edb_10069.png)
## EDB Postgres Advanced Server Installation Wizard - Additional Directories
![](./images/edb_10070.png)
## EDB Postgres Advanced Server Installation Wizard - Configuration Mode
![](./images/edb_10071.png)
## EDB Postgres Advanced Server Installation Wizard - Password
![](./images/edb_10072.png)
## EDB Postgres Advanced Server Installation Wizard - Additional Configuration
![](./images/edb_10073.png)
## EDB Postgres Advanced Server Installation Wizard - Server Utilization
![](./images/edb_10074.png)
## EDB Postgres Advanced Server Installation Wizard - Workload Profile
![](./images/edb_10075.png)
## EDB Postgres Advanced Server Installation Wizard - Advanced Configuration
![](./images/edb_10076.png)
## EDB Postgres Advanced Server Installation Wizard - Summary
![](./images/edb_10077.png)
## EDB Postgres Advanced Server Installation Wizard - Finish
![](./images/edb_10078.png)
## Demo - Wizard Installation
![](./images/edb_10079.png)
## Acces StackBuilder Plus
![](./images/edb_10080.png)
## StackBuilder Plus - Welcome
![](./images/edb_10081.png)
## StackBuilder Plus - Select Applications
![](./images/edb_10082.png)
## Demo - StackBuilder Plus
![](./images/edb_10083.png)
## Text Mode Wizard Installation
![](./images/edb_10084.png)
## Prepare for Text Mode Installation
![](./images/edb_10085.png)
## Text Mode Installation
![](./images/edb_10086.png)
## Text Mode Installation Steps
![](./images/edb_10087.png)
## Text Mode Installation Summary
![](./images/edb_10088.png)
## RPM Installation
![](./images/edb_10089.png)
## RPM Installation Pre-Requisites
![](./images/edb_10090.png)
## RPM Package Manager
![](./images/edb_10091.png)
## YUM Installation
![](./images/edb_10092.png)
## Configure Yum Package Manager
![](./images/edb_10093.png)
## Install EDB Postgres Advanced Server using Yum
![](./images/edb_10094.png)
## After Installation
![](./images/edb_10095.png)
## Setting Environmental Variables
![](./images/edb_10096.png)
## Example - Setting Environment Variables
![](./images/edb_10097.png)
## Demo - Setup Enviromental Variables
![](./images/edb_10098.png)
## Module Summary
![](./images/edb_10099.png)
## Lab Exercise - 1
![](./images/edb_10100.png)
