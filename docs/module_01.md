## Module 1 - Introduction
![](./images/edb_10003.png)
## Module Objectives
![](./images/edb_10004.png)
## EDB Postgres Platform
![](./images/edb_10005.png)
## EDB Postgres Platform - DBMS Options
![](./images/edb_10006.png)
## EDB Postgres Platform - Tool Suites
![](./images/edb_10007.png)
## EDB Postgres Platform - Deployment Options
![](./images/edb_10008.png)
## EDB Postgres Platform - Services & Support
![](./images/edb_10009.png)
## The EDB Postgres Platform - Subscriptions
![](./images/edb_10010.png)
## EDB Postgres Ark
![](./images/edb_10011.png)
## Database Platform For Digital Business
![](./images/edb_10012.png)
## Facts about PostgreSQL
![](./images/edb_10013.png)
## Facts about EDB Postgres Advanced Server
![](./images/edb_10014.png)
## EDB Postgres Advanced Server Version History
![](./images/edb_10015.png)
## Major Features of PostgreSQL
![](./images/edb_10016.png)
## Major Features of PostgreSQL (continued)
![](./images/edb_10017.png)
## EDB Postgres Advanced Server
![](./images/edb_10018.png)
## EDB Postgres Advanced Server - Security
![](./images/edb_10019.png)
## EDB Postgres Advanced Server - Bundled Tools
![](./images/edb_10020.png)
## EDB Postgres Advanced Server - Performance
![](./images/edb_10021.png)
## EDB Postgres Advanced Server - Database Compatibility for Oracle
![](./images/edb_10022.png)
## EDB Technologies
![](./images/edb_10023.png)
## EDB DBaaS Framework For Hybrid Clouds
![](./images/edb_10024.png)
## General Database Limits
![](./images/edb_10025.png)
## Comon Database Object Names
![](./images/edb_10026.png)
## Module Summary
![](./images/edb_10027.png)