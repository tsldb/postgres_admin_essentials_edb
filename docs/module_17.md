## Module 17 - Oracle Compatibility
![](./images/edb_10433.png)
## Module Objectives
![](./images/edb_10434.png)
## Oracle Compatibility
![](./images/edb_10435.png)
## Oracle Compatible Tools
![](./images/edb_10436.png)
## Oracle Compatibility Settings
![](./images/edb_10437.png)
## Example - Oracle Compatibility Settings
![](./images/edb_10438.png)
## Oracle Compatibility - Data Types
![](./images/edb_10439.png)
## Oracle Compatibility - Synonyms
![](./images/edb_10440.png)
## Dual Table
![](./images/edb_10441.png)
## Oracle Compatibility - Altering Session Parameters
![](./images/edb_10442.png)
## Oracle Compatibility - Database Link
![](./images/edb_10443.png)
## Oracle Compatibility - Create Directory
![](./images/edb_10444.png)
## Hierarchical Queries
![](./images/edb_10445.png)
## Oracle Compatible Connect By
![](./images/edb_10446.png)
## Table Partitioning
![](./images/edb_10447.png)
## Stored Procedure Language
![](./images/edb_10448.png)
## Built-in Packages
![](./images/edb_10449.png)
## System Catalog Views
![](./images/edb_10450.png)
## sys Schema
![](./images/edb_10451.png)
## ECPGPlus
![](./images/edb_10452.png)
## Open Client Library(OCL)
![](./images/edb_10453.png)
## Other Oracle Compatibilities
![](./images/edb_10454.png)
## Module Summary
![](./images/edb_10455.png)
## Lab Exercise
![](./images/edb_10456.png)
## Lab Exercise (continued)
![](./images/edb_10457.png)