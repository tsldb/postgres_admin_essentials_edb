## Module 2 - System Architecture
![](./images/edb_10028.png)
## Module Objectives
![](./images/edb_10029.png)
## Architectural Summary
![](./images/edb_10030.png)
## Proces and Memory Architecture
![](./images/edb_10031.png)
## Utility Proceses
![](./images/edb_10032.png)
## More Utility Proces
![](./images/edb_10033.png)
## Postmaster as Listener
![](./images/edb_10034.png)
## User Back End Process
![](./images/edb_10035.png)
## Respond to Client
![](./images/edb_10036.png)
## Demo -List of Processes
![](./images/edb_10037.png)
## Disk Read Buffering
![](./images/edb_10038.png)
## Disk Write Buffering
![](./images/edb_10039.png)
## Background Writer Cleaning Scan
![](./images/edb_10040.png)
## Write Ahead Logging (WAL)
![](./images/edb_10041.png)
## Transaction Log Archiving
![](./images/edb_10042.png)
## Commit and Checkpoint
![](./images/edb_10043.png)
## Statement Processing
![](./images/edb_10044.png)
## Physical Database Architecture
![](./images/edb_10045.png)
## Installation Directory Layout
![](./images/edb_10046.png)
## Database Cluster Data Directory Layout
![](./images/edb_10047.png)
## Physical Database Architecture
![](./images/edb_10048.png)
## Demo - Data Directory
![](./images/edb_10049.png)
## Sample - Data Directory Layout
![](./images/edb_10050.png)
## Page Layout
![](./images/edb_10051.png)
## Page Structure
![](./images/edb_10052.png)
## Module Summary
![](./images/edb_10053.png)
