## Module 12 - Routine Maintenance Tasks
![](./images/edb_10343.png)
## Module Objectives
![](./images/edb_10344.png)
## Database Maintenance
![](./images/edb_10345.png)
## Maintenance Tools
![](./images/edb_10346.png)
## Optimizer Statistics
![](./images/edb_10347.png)
## Example - Updating Statistics
![](./images/edb_10348.png)
## Demo - Optimizer Statistics
![](./images/edb_10349.png)
## Data Fragmentation and Bloat
![](./images/edb_10350.png)
## Routine Vacuuming
![](./images/edb_10351.png)
## Vacuuming Commands
![](./images/edb_10352.png)
## Vacuum and Vacuum Full
![](./images/edb_10353.png)
## VACUUM Syntax
![](./images/edb_10354.png)
## Example - Vacuuming
![](./images/edb_10355.png)
## Example - Vacuuming (continued)
![](./images/edb_10356.png)
## Demo - Vacuum Command
![](./images/edb_10357.png)
## Preventing Transaction ID Wraparound Failures
![](./images/edb_10358.png)
## Vacum Freeze
![](./images/edb_10359.png)
## The Visibility Map
![](./images/edb_10360.png)
## vacuumdb Utility
![](./images/edb_10361.png)
## Demo - vacuumdb Utility
![](./images/edb_10362.png)
## Autovacuuming
![](./images/edb_10363.png)
## Autovacuuming Parameters
![](./images/edb_10364.png)
## Per-Table Thresholds
![](./images/edb_10365.png)
## Routine Reindexing
![](./images/edb_10366.png)
## When to Reindex
![](./images/edb_10367.png)
## Demo - Reindexing
![](./images/edb_10368.png)
## CLUSTER
![](./images/edb_10369.png)
## Example - CLUSTER
![](./images/edb_10370.png)
## Module Summary
![](./images/edb_10371.png)
## Lab Exercise - 1
![](./images/edb_10372.png)
## Lab Exercise - 2
![](./images/edb_10373.png)