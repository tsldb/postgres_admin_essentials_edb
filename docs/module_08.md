## Module 8 - GUI Tools
![](./images/edb_10200.png)
## Module Objectives
![](./images/edb_10201.png)
## Overview
![](./images/edb_10202.png)
## Features
![](./images/edb_10203.png)
## Download PEM Client
![](./images/edb_10204.png)
## Installing PEM Client
![](./images/edb_10205.png)
## PEM Client - License Agreement
![](./images/edb_10206.png)
## PEM Client - Installation Directory
![](./images/edb_10207.png)
## PEM Client - Finish Installation
![](./images/edb_10208.png)
## Demo - Installing PEM Client
![](./images/edb_10209.png)
## Open Postgres Enterprise Manager Client
![](./images/edb_10210.png)
## PEM Client First Look
![](./images/edb_10211.png)
## Register A Database Cluster
![](./images/edb_10212.png)
## Common Connection Problems
![](./images/edb_10213.png)
## Changing A Server's Registration
![](./images/edb_10214.png)
## Viewing Data
![](./images/edb_10215.png)
## Filtering and Sorting Data
![](./images/edb_10216.png)
## Query Tool
![](./images/edb_10217.png)
## Query Tool - Change Connection
![](./images/edb_10218.png)
## Query Tool - Data Output
![](./images/edb_10219.png)
## Query Tool - Graphical Query Builder
![](./images/edb_10220.png)
## Query Tool - Explain
![](./images/edb_10221.png)
## Query Tool - Messages
![](./images/edb_10222.png)
## Query Tool - History
![](./images/edb_10223.png)
## Databases
![](./images/edb_10224.png)
## Creating a Database
![](./images/edb_10225.png)
## Schemas
![](./images/edb_10226.png)
## Tables
![](./images/edb_10227.png)
## Tables - Adding Columns/Constraints
![](./images/edb_10228.png)
## Tables - Indexes
![](./images/edb_10229.png)
## Tables - Maintenance
![](./images/edb_10230.png)
## Tablespaces
![](./images/edb_10231.png)
## Roles
![](./images/edb_10232.png)
## Server Status
![](./images/edb_10233.png)
## Demo - PEM Client
![](./images/edb_10234.png)
## Module Summary
![](./images/edb_10235.png)
## Lab Exercise - 1
![](./images/edb_10236.png)
## Lab Exercise - 1 (continued)
![](./images/edb_10237.png)