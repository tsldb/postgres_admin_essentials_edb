## Module 13 - Data Dictionary
![](./images/edb_10374.png)
## Module Objectives
![](./images/edb_10375.png)
## The System Catalog Schema
![](./images/edb_10376.png)
## System Information Tables
![](./images/edb_10377.png)
## More System Information Tables
![](./images/edb_10378.png)
## System Information Functions
![](./images/edb_10379.png)
## System Administration Functions
![](./images/edb_10380.png)
## More System Administration Functions
![](./images/edb_10381.png)
## System Information Views
![](./images/edb_10382.png)
## Demo - System Catalogs
![](./images/edb_10383.png)
## EDB Postgres Advanced Server Oracle-Like Dictionaries
![](./images/edb_10384.png)
## Module Summary
![](./images/edb_10385.png)
## Lab Exercise - 1
![](./images/edb_10386.png)
## Lab Exercise - 2
![](./images/edb_10387.png)
## Lab Exercise - 3
![](./images/edb_10388.png)
## Lab Exercise - 4
![](./images/edb_10389.png)