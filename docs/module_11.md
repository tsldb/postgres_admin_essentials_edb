## Module 11 - Backup, Recovery and PITR
![](./images/edb_10307.png)
## Module Objectives
![](./images/edb_10308.png)
## Types of Backup
![](./images/edb_10309.png)
## Database SQL Dump
![](./images/edb_10310.png)
## pg_dump Options
![](./images/edb_10311.png)
## Demo - SQL Dump Backups
![](./images/edb_10312.png)
## SQL Dump - Large Databases
![](./images/edb_10313.png)
## Restore - SQL Dump
![](./images/edb_10314.png)
## pg_restore Options
![](./images/edb_10315.png)
## Demo - SQL Dump Restore
![](./images/edb_10316.png)
## Entire Cluster - SQL Dump
![](./images/edb_10317.png)
## pg_dumpall Options
![](./images/edb_10318.png)
## Demo - Cluster Dump
![](./images/edb_10319.png)
## Backup - File System Level Backup
![](./images/edb_10320.png)
## Demo - Offline Filesystem Level Backups
![](./images/edb_10321.png)
## Backup - Continuous Archiving
![](./images/edb_10322.png)
## pg_receivexlog
![](./images/edb_10323.png)
## Continuous Archiving
![](./images/edb_10324.png)
## Demo - Continuous Archiving
![](./images/edb_10325.png)
## Base Backup Using Low Level API
![](./images/edb_10326.png)
## Base Backup Using pg_basebackup Tool
![](./images/edb_10327.png)
## Options for pg_basebackup
![](./images/edb_10328.png)
## pg_basebackup - Online Backup
![](./images/edb_10329.png)
## Demo - On Line Base Backups
![](./images/edb_10330.png)
## Point-in-Time Recovery
![](./images/edb_10331.png)
## Performing Point-in-Time Recovery
![](./images/edb_10332.png)
## Point-in-Time Recovery Settings
![](./images/edb_10333.png)
## Demo - Point-in-Time Recovery
![](./images/edb_10334.png)
## EDB Backup and Recovery Tool
![](./images/edb_10335.png)
## Module Summary
![](./images/edb_10336.png)
## Lab Exercise - 1
![](./images/edb_10337.png)
## Lab Exercise - 2
![](./images/edb_10338.png)
## Lab Exercise - 3
![](./images/edb_10339.png)
## Lab Exercise - 4
![](./images/edb_10340.png)
## Lab Exercise -5
![](./images/edb_10341.png)
## Lab Exercise -6
![](./images/edb_10342.png)