## Module 9 - Security
![](./images/edb_10238.png)
## Module Objectives
![](./images/edb_10239.png)
## Authentication and Authorization
![](./images/edb_10240.png)
## Levels of Security
![](./images/edb_10241.png)
## pg_hba.conf - Acces Control
![](./images/edb_10242.png)
## pg_hba.conf Example
![](./images/edb_10243.png)
## Demo - pg_hba.conf
![](./images/edb_10244.png)
## Authentication Problems
![](./images/edb_10245.png)
## Row Level Security (RLS)
![](./images/edb_10246.png)
## Example - Row Level Security
![](./images/edb_10247.png)
## Example - Row Level Security (continued)
![](./images/edb_10248.png)
## Application Access
![](./images/edb_10249.png)
## SQL/Protect
![](./images/edb_10250.png)
## EDB*Wrap
![](./images/edb_10251.png)
## Module Summary
![](./images/edb_10252.png)
## Lab Exercise - 1
![](./images/edb_10253.png)
## Lab Exercise - 2
![](./images/edb_10254.png)
## Lab Exercise - 3
![](./images/edb_10255.png)
## Lab Exercise - 4
![](./images/edb_10256.png)