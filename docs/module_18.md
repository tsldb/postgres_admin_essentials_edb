## Module 18 - Migrating Oracle Objects to EDB Postgres Advanced Server
![](./images/edb_10458.png)
## Module Objectives
![](./images/edb_10459.png)
## Oracle to EDB Postgres Advanced Server Migration
![](./images/edb_10460.png)
## Migration Process
![](./images/edb_10461.png)
## Migration Assessment
![](./images/edb_10462.png)
## Assessing Migrations - Identify Candidate Databases
![](./images/edb_10463.png)
## Migration Assessment Methodology
![](./images/edb_10464.png)
## Database Migration Assessment Tool (DMAT)
![](./images/edb_10465.png)
## DMAT - Features
![](./images/edb_10466.png)
## DMAT - Architecture
![](./images/edb_10467.png)
## DMAT - Report
![](./images/edb_10468.png)
## Reading the Migration Assessment Report
![](./images/edb_10469.png)
## DMAT - Available Solutions
![](./images/edb_10470.png)
## Demo - DMAT
![](./images/edb_10471.png)
## Database Migration
![](./images/edb_10472.png)
## Database Migration - Preparation
![](./images/edb_10473.png)
## Information Collection
![](./images/edb_10474.png)
## EDB Postgres Migration Toolkit
![](./images/edb_10475.png)
## EDB Postgres Migration Toolkit - Features
![](./images/edb_10476.png)
## EDB Postgres Migration Toolkit - Objects Matrix
![](./images/edb_10477.png)
## EDB Postgres Migration Toolkit - Installation
![](./images/edb_10478.png)
## EDB Postgres Migration Toolkit - Graphical Installation
![](./images/edb_10479.png)
## EDB Postgres Migration Toolkit - StackBuilder Installation
![](./images/edb_10480.png)
## EDB Postgres Migration Toolkit - Select Target Server
![](./images/edb_10481.png)
## EDB Postgres Migration Toolkit - Application Selection
![](./images/edb_10482.png)
## EDB Postgres Migration Toolkit - Installation Steps
![](./images/edb_10483.png)
## Demo - Installing MTK
![](./images/edb_10484.png)
## EDB Postgres Migration Toolkit - Source-Specific Drivers
![](./images/edb_10485.png)
## Example - Source-Specific Drivers
![](./images/edb_10486.png)
## Demo -Source Specific Drivers
![](./images/edb_10487.png)
## EDB Postgres Migration Toolkit - Prepare a Connection
![](./images/edb_10488.png)
## EDB Postgres Migration Toolkit - Perform Migration
![](./images/edb_10489.png)
## EDB Postgres Migration Toolkit Options
![](./images/edb_10490.png)
## EDB Postgres Migration Toolkit Options (Continued)
![](./images/edb_10491.png)
## EDB Postgres Migration Toolkit - runMTK Command
![](./images/edb_10492.png)
## EDB Postgres Migration Toolkit - What's Migrated
![](./images/edb_10493.png)
## EDB Postgres Migration Toolkit - Offline Migration
![](./images/edb_10494.png)
## EDB Postgres Migration Toolkit - What's Migrated
![](./images/edb_10495.png)
## Demo - Migration
![](./images/edb_10496.png)
## Migration Toolkit - Looking for Errors
![](./images/edb_10497.png)
## Application Migration
![](./images/edb_10498.png)
## Application Migration
![](./images/edb_10499.png)
## Deployment
![](./images/edb_10500.png)
## Deployment
![](./images/edb_10501.png)
## Deploy - Postgres Enterprise Manager
![](./images/edb_10502.png)
## Ongoing Management and Optimization
![](./images/edb_10503.png)
## EDB Migration Methodology
![](./images/edb_10504.png)
## Module Summary
![](./images/edb_10505.png)
## Lab Exercise
![](./images/edb_10506.png)