## Module 6 - Creating and Managing Databases
![](./images/edb_10144.png)
## Module Objectives
![](./images/edb_10145.png)
## Object Hierarchy
![](./images/edb_10146.png)
## Demo - Default Hierarchy
![](./images/edb_10147.png)
## What Is A Database?
![](./images/edb_10148.png)
## Creating Databases
![](./images/edb_10149.png)
## Example - Creating Databases
![](./images/edb_10150.png)
## Accessing a Database
![](./images/edb_10151.png)
## Demo - Creating Databases
![](./images/edb_10152.png)
## Users
![](./images/edb_10153.png)
## Creating Users using psql or edb-psql
![](./images/edb_10154.png)
## Creating Users Using EDB*Plus
![](./images/edb_10155.png)
## Creating Users Using createuser
![](./images/edb_10156.png)
## User Profile Management
![](./images/edb_10157.png)
## Creating A User Profile
![](./images/edb_10158.png)
## Privileges
![](./images/edb_10159.png)
## GRANT Statement
![](./images/edb_10160.png)
## Example - GRANT Statement
![](./images/edb_10161.png)
## REVOKE Statement
![](./images/edb_10162.png)
## Example - REVOKE Statement
![](./images/edb_10163.png)
## Demo - Users and Permissions
![](./images/edb_10164.png)
## What Is A Schema?
![](./images/edb_10165.png)
## Benefits of Schemas
![](./images/edb_10166.png)
## Creating Schemas
![](./images/edb_10167.png)
## Demo - Creating Schemas
![](./images/edb_10168.png)
## What Is A Schema Search Path?
![](./images/edb_10169.png)
## Determine The Schema Search Path
![](./images/edb_10170.png)
## Schema Search Path and Users
![](./images/edb_10171.png)
## Set The Schema Search Path
![](./images/edb_10172.png)
## Demo - Search Path
![](./images/edb_10173.png)
## Object Ownership
![](./images/edb_10174.png)
## Module Summary
![](./images/edb_10175.png)
## Lab Exercise - 1
![](./images/edb_10176.png)
## Lab Exercise - 2
![](./images/edb_10177.png)
## Lab Exercise - 3
![](./images/edb_10178.png)
## Demo - Sample Tables
![](./images/edb_10179.png)
## Lab Exercise - 4
![](./images/edb_10180.png)
