## Module 10 - SQL Primer
![](./images/edb_10257.png)
## Module Objectives
![](./images/edb_10258.png)
## Data Types
![](./images/edb_10259.png)
## Structured Query Language
![](./images/edb_10260.png)
## Demo - Data Types and SQL Commands
![](./images/edb_10261.png)
## Tables
![](./images/edb_10262.png)
## Constraints
![](./images/edb_10263.png)
## Types of Constraints
![](./images/edb_10264.png)
## Example - CREATE TABLE
![](./images/edb_10265.png)
## Example - SERIAL Data Type
![](./images/edb_10266.png)
## ALTER TABLE
![](./images/edb_10267.png)
## DROP TABLE
![](./images/edb_10268.png)
## Demo - Create, Drop and Alter Table
![](./images/edb_10269.png)
## Inserting Data
![](./images/edb_10270.png)
## Example - INSERT
![](./images/edb_10271.png)
## Demo - Insert
![](./images/edb_10272.png)
## Updating Rows
![](./images/edb_10273.png)
## Example - UPDATE
![](./images/edb_10274.png)
## Demo - Update
![](./images/edb_10275.png)
## How to Upsert in EDB Postgres Advanced Server
![](./images/edb_10276.png)
## Deleting Data
![](./images/edb_10277.png)
## Example - DELETE
![](./images/edb_10278.png)
## Demo - Delete
![](./images/edb_10279.png)
## Views
![](./images/edb_10280.png)
## Example - View
![](./images/edb_10281.png)
## Demo - Views
![](./images/edb_10282.png)
## Materialized Views
![](./images/edb_10283.png)
## Materialized View Example
![](./images/edb_10284.png)
## Sequences
![](./images/edb_10285.png)
## Demo - Sequences
![](./images/edb_10286.png)
## Domains
![](./images/edb_10287.png)
## Example - Domain
![](./images/edb_10288.png)
## Demo - Domains
![](./images/edb_10289.png)
## Quoting
![](./images/edb_10290.png)
## Demo - Quoting
![](./images/edb_10291.png)
## Using SQL Functions
![](./images/edb_10292.png)
## Format Functions
![](./images/edb_10293.png)
## Concatenating Strings
![](./images/edb_10294.png)
## Nested SELECT Statements
![](./images/edb_10295.png)
## Inner Joins
![](./images/edb_10296.png)
## Column and Table Aliases
![](./images/edb_10297.png)
## Outer Joins
![](./images/edb_10298.png)
## Indexes
![](./images/edb_10299.png)
## Example Index
![](./images/edb_10300.png)
## Module Summary
![](./images/edb_10301.png)
## Lab Exercise - 1
![](./images/edb_10302.png)
## Lab Exercise - 2
![](./images/edb_10303.png)
## Lab Exercise - 3
![](./images/edb_10304.png)
## Lab Exercise - 4
![](./images/edb_10305.png)
## Lab Exercise - 5
![](./images/edb_10306.png)