## Module15 - EDB*Plus
![](./images/edb_10404.png)
## Module Objectives
![](./images/edb_10405.png)
## EDB*Plus
![](./images/edb_10406.png)
## EDB*Plus Features
![](./images/edb_10407.png)
## Starting EDB*Plus
![](./images/edb_10408.png)
## Demo - Starting EDB*Plus
![](./images/edb_10409.png)
## EDB*Plus SET Command
![](./images/edb_10410.png)
## EDB*Plus Commands
![](./images/edb_10411.png)
## List of EDB*Plus Commands
![](./images/edb_10412.png)
## Setup Login Variable
![](./images/edb_10413.png)
## Demo - Login Variables
![](./images/edb_10414.png)
## Calling a SQL Script
![](./images/edb_10415.png)
## Module Summary
![](./images/edb_10416.png)
## Lab Exercise
![](./images/edb_10417.png)