## Module16 - Tablespaces
![](./images/edb_10418.png)
## Module Objectives
![](./images/edb_10419.png)
## Tablespaces and Data Files
![](./images/edb_10420.png)
## Advantages of Tablespaces
![](./images/edb_10421.png)
## EDB Postgres Advanced Server Pre-Configured Tablespaces
![](./images/edb_10422.png)
## Creating Tablespaces
![](./images/edb_10423.png)
## Example - CREATE TABLESPACE
![](./images/edb_10424.png)
## Using Tablespaces
![](./images/edb_10425.png)
## Default and Temp Tablespace
![](./images/edb_10426.png)
## Altering Tablespaces
![](./images/edb_10427.png)
## Example - Alter Tabespace
![](./images/edb_10428.png)
## Dropping a Tablespace
![](./images/edb_10429.png)
## Example - Dropping Tablespaces
![](./images/edb_10430.png)
## Module Summary
![](./images/edb_10431.png)
## Lab Exercise
![](./images/edb_10432.png)