mkdocs build --clean

mkdocs2pandoc > enterprisedb_basiccourse_text.pd
# Generate PDF
pandoc --latex-engine=xelatex --toc -f markdown+grid_tables+table_captions -o enterprisedb_basiccourse_text.pdf enterprisedb_basiccourse_text.pd

